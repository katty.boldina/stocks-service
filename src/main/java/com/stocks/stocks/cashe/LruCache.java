package com.stocks.stocks.cashe;

import com.stocks.stocks.dto.StockInfo;

import java.util.LinkedHashMap;
import java.util.Map;

public class LruCache<K, V> extends LinkedHashMap<String, StockInfo> {
    private static final int MAX_ENTRIES = 5;

    protected boolean removeEldestEntry(Map.Entry<String, StockInfo> eldest) {
        return size() > MAX_ENTRIES;
    }
}
