package com.stocks.stocks.cashe;

import com.stocks.stocks.dto.StockInfo;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.springframework.stereotype.Component;

@Slf4j
@Aspect
@Component
@RequiredArgsConstructor
public class CacheProcessor {

    LruCache<String, StockInfo> stockCache = new LruCache<>();

    @Around("@annotation(com.stocks.stocks.cashe.LruCasheable)")
    public Object aroundAuditFeignMethodAdvice(ProceedingJoinPoint point) throws Throwable {
        String ticker = (String) point.getArgs()[0];

        //if cache contains info by ticker, then return from cache
        if (stockCache.containsKey(ticker)) {
            StockInfo response = stockCache.get(ticker);
            stockCache.remove(ticker);
            stockCache.put(ticker, response);
            return response;
        }

        Object body = point.proceed();

        if (body instanceof StockInfo) {
            StockInfo response = (StockInfo) body;
            stockCache.put(response.getTickerSymbol(), response);
        }
        return body;
    }
}
