package com.stocks.stocks.exceptions;

public class TickerNotFoundException extends RuntimeException {
    private static final long serialVersionUID = -9207682517252854404L;

    public TickerNotFoundException(String tickerSymbol) {
        super(String.format("Ticker not found %s", tickerSymbol));
    }
}
