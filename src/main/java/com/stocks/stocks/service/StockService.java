package com.stocks.stocks.service;

import com.stocks.stocks.dto.CreateStockDto;
import com.stocks.stocks.dto.StockInfo;
import com.stocks.stocks.entities.StockEntity;

import java.util.Optional;

public interface StockService {
    /**
     * Get stock info by ticker symbol.
     *
     * @param tickerSymbol ticker symbol
     * @return {@link StockInfo}
     */
    StockInfo getStockInfo(String tickerSymbol);

    /**
     * Create stock
     *
     * @param dto {@link CreateStockDto}
     */
    void createStock(CreateStockDto dto);

    /**
     * Get stock by ticker.
     *
     * @param ticker ticker
     * @return {@link Optional} of {@link StockEntity}
     */
    Optional<StockEntity> getByTicker(String ticker);
}
