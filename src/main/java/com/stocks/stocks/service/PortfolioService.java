package com.stocks.stocks.service;

import com.stocks.stocks.dto.AddStocksResponse;
import com.stocks.stocks.dto.StocksToAddDto;

import java.util.UUID;

public interface PortfolioService {
    /**
     * Add stocks to portfolio.
     *
     * @param stocksToAddDto {@link StocksToAddDto}
     * @param portfolioId    portfolio id
     * @return {@link AddStocksResponse}
     */
    AddStocksResponse addStocksToPortfolio(StocksToAddDto stocksToAddDto, UUID portfolioId);
}
