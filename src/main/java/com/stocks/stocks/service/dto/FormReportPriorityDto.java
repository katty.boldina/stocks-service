package com.stocks.stocks.service.dto;

import com.stocks.stocks.dto.Company;
import lombok.Data;

@Data
public class FormReportPriorityDto {
    private Company company;
    private int priority;
}
