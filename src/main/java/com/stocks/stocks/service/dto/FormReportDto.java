package com.stocks.stocks.service.dto;

import com.stocks.stocks.dto.Company;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FormReportDto {
    private Company company;
}
