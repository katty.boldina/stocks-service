package com.stocks.stocks.service.impl;

import com.stocks.stocks.dto.AddStocksResponse;
import com.stocks.stocks.dto.FailedToAddStock;
import com.stocks.stocks.dto.StocksToAddDto;
import com.stocks.stocks.dto.Tariff;
import com.stocks.stocks.entities.PortfolioEntity;
import com.stocks.stocks.entities.PortfolioStockEntity;
import com.stocks.stocks.entities.StockEntity;
import com.stocks.stocks.entities.TariffEntity;
import com.stocks.stocks.repositories.PortfolioRepository;
import com.stocks.stocks.repositories.PortfolioStockRepository;
import com.stocks.stocks.repositories.StockRepository;
import com.stocks.stocks.repositories.TariffRepository;
import com.stocks.stocks.service.PortfolioService;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.UUID;
import java.util.stream.Collectors;

@Slf4j
@Service
@RequiredArgsConstructor
public class PortfolioServiceImpl implements PortfolioService {

    private final StockRepository stockRepository;
    private final PortfolioRepository portfolioRepository;
    private final PortfolioStockRepository portfolioStockRepository;
    private final TariffRepository tariffRepository;

    @Override
    public AddStocksResponse addStocksToPortfolio(StocksToAddDto stocksToAddDto, UUID portfolioId) {

        List<FailedToAddStock> failedToAddStocks = new ArrayList<>();
        TreeSet<StockEntity> stocksToAddWithTotalPrice = new TreeSet<>(Comparator.comparing(StockEntity::getTotalPrice));

        PortfolioEntity portfolio = this.portfolioRepository.getById(portfolioId);
        List<PortfolioStockEntity> portfolioStockList = this.portfolioStockRepository.getAllByPortfolio(portfolio);

        int currentNumberOfStocks = portfolioStockList.size();

        populateStockMap(stocksToAddDto, failedToAddStocks, stocksToAddWithTotalPrice);

        if (Tariff.ENTERPRISE.equals(portfolio.getTariff().getName())) {

            Set<PortfolioStockEntity> stocks = new HashSet<>();
            BigDecimal newPortfolioTotalPrice = BigDecimal.valueOf(0);

            for (StockEntity stockEntity : stocksToAddWithTotalPrice) {
                newPortfolioTotalPrice = addPortfolioStockAndReturnTotalPrice(portfolio, stocks, newPortfolioTotalPrice, stockEntity);

            }

            this.portfolioStockRepository.saveAll(stocks);
            portfolio.setTotalPrice(newPortfolioTotalPrice);
            this.portfolioRepository.save(portfolio);

        } else {

            Optional<TariffEntity> tariffOptional = this.tariffRepository.getByName(Tariff.ESSENTIALS);
            if (tariffOptional.isEmpty()) {
                throw new IllegalArgumentException(); //TODO add custom exception
            }
            TariffEntity tariff = tariffOptional.get();

            int numberOfStocksToAdd = tariff.getStocksNumber() - currentNumberOfStocks;
            boolean canAddMoreStocks = numberOfStocksToAdd > 0;

            if (canAddMoreStocks) {
                Set<PortfolioStockEntity> stocks = new HashSet<>();
                BigDecimal newPortfolioTotalPrice = BigDecimal.valueOf(0);
                for (int i = numberOfStocksToAdd; i > 0; i--) {
                    StockEntity stock = stocksToAddWithTotalPrice.pollLast();

                    newPortfolioTotalPrice = addPortfolioStockAndReturnTotalPrice(portfolio, stocks, newPortfolioTotalPrice, stock);
                }

                this.portfolioStockRepository.saveAll(stocks);
                portfolio.setTotalPrice(newPortfolioTotalPrice);
                this.portfolioRepository.save(portfolio);

                //обрабатываем оставшиеся активы
                Set<FailedToAddStock> restStocks = stocksToAddWithTotalPrice.stream()
                        .map(stock -> new FailedToAddStock(
                                stock.getTicker(),
                                stock.getLotsNumber(),
                                "Can not add because if tariff"))
                        .collect(Collectors.toSet());

                failedToAddStocks.addAll(restStocks);
            }
        }


        return null;
    }

    private BigDecimal addPortfolioStockAndReturnTotalPrice(PortfolioEntity portfolio,
                                                            Set<PortfolioStockEntity> stocks,
                                                            BigDecimal newPortfolioTotalPrice,
                                                            StockEntity stockEntity) {
        PortfolioStockEntity portfolioStock = new PortfolioStockEntity();
        portfolioStock.setLotAmount(stockEntity.getLotsNumber());
        portfolioStock.setPortfolio(portfolio);
        portfolioStock.setStock(stockEntity);
        portfolioStock.setTotalPrice(stockEntity.getTotalPrice());
        stocks.add(portfolioStock);

        newPortfolioTotalPrice = newPortfolioTotalPrice.add(stockEntity.getTotalPrice());
        return newPortfolioTotalPrice;
    }

    private void populateStockMap(StocksToAddDto stocksToAddDto,
                                  List<FailedToAddStock> failedToAddStocks,
                                  Set<StockEntity> stocksToAddWithTotalPrice) {
        stocksToAddDto.getStocksToAdds().forEach(stockToAdd -> {
            Optional<StockEntity> stockOptional = stockRepository.findByTicker(stockToAdd.getTicker());
            if (stockOptional.isEmpty()) {
                failedToAddStocks.add(new FailedToAddStock(
                        stockToAdd.getTicker(),
                        stockToAdd.getLotsNumber(),
                        "Ticker could not be found"
                ));
            } else {
                StockEntity stock = stockOptional.get();
                BigDecimal totalPriceForCurrentStock = BigDecimal.valueOf(stockToAdd.getLotsNumber())
                        .multiply(stock.getPrice());
                stock.setLotsNumber(stockToAdd.getLotsNumber());
                stock.setTotalPrice(totalPriceForCurrentStock);
                stocksToAddWithTotalPrice.add(stock);
            }
        });
    }
}
