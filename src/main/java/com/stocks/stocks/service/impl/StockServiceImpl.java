package com.stocks.stocks.service.impl;

import com.stocks.stocks.cashe.LruCasheable;
import com.stocks.stocks.dto.CreateStockDto;
import com.stocks.stocks.dto.StockInfo;
import com.stocks.stocks.entities.StockEntity;
import com.stocks.stocks.exceptions.TickerNotFoundException;
import com.stocks.stocks.repositories.StockRepository;
import com.stocks.stocks.service.StockService;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Slf4j
@Service
@AllArgsConstructor
public class StockServiceImpl implements StockService {

    private StockRepository stockRepository;

    @Override
    @LruCasheable(methodName = "getStockInfo")
    public StockInfo getStockInfo(String tickerSymbol) {
        log.debug("Start getting stock info by ticker: {}", tickerSymbol);

        StockEntity stockEntity = this.stockRepository.findByTicker(tickerSymbol)
                .orElseThrow(() -> new TickerNotFoundException(tickerSymbol));

        StockInfo stockInfo = new StockInfo(
                stockEntity.getTicker(),
                stockEntity.getPrice(),
                stockEntity.getLot(),
                null
//                stockEntity.getCompany().getCountry().getName()
        );

        log.debug("Got stock info by ticker: {}, info: {}", tickerSymbol, stockInfo);
        return stockInfo;
    }

    @Override
    public void createStock(CreateStockDto dto) {
        StockEntity stockEntity = new StockEntity();
//        stockEntity.setCountry(dto.getCountry()); TODO
        stockEntity.setLot(dto.getLot());
        stockEntity.setPrice(dto.getPrice());
//        stockEntity.getTicker(dto.getTickerSymbol()); TODO
        this.stockRepository.save(stockEntity);
    }

    @Override
    public Optional<StockEntity> getByTicker(String ticker) {
        return this.stockRepository.findByTicker(ticker);
    }
}
