package com.stocks.stocks.service.impl;


import com.stocks.stocks.service.JasperService;
import com.stocks.stocks.service.dto.FormReportDto;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

@Slf4j
@Service
public class JasperServiceImpl implements JasperService {
    @Override
    public byte[] createCompanyReport(FormReportDto formReportDto) {
        return new byte[0];
    }
}
