package com.stocks.stocks.service.impl;

import com.stocks.stocks.dto.ReportByCountry;
import com.stocks.stocks.dto.request.GetReportPriorityRequestDto;
import com.stocks.stocks.dto.request.GetReportRequestDto;
import com.stocks.stocks.entities.PortfolioEntity;
import com.stocks.stocks.entities.StockEntity;
import com.stocks.stocks.repositories.PortfolioRepository;
import com.stocks.stocks.service.ProcessReportRequestService;
import com.stocks.stocks.service.ReportService;
import com.stocks.stocks.service.StockService;
import com.stocks.stocks.service.dto.FormReportDto;
import com.stocks.stocks.service.dto.FormReportPriorityDto;
import lombok.AllArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import java.math.BigDecimal;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

@Slf4j
@Service
@AllArgsConstructor
public class ReportServiceImpl implements ReportService {

    private final ProcessReportRequestService processReportRequestService;
    private final PortfolioRepository portfolioRepository;
    private final StockService stockService;

    @Override
    public void formCompanyReport(GetReportRequestDto reportRequestDto) {
        log.debug("Start sending request to form company report, for company: {}", reportRequestDto.getCompany());

        FormReportDto formReportDto = new FormReportDto();
        formReportDto.setCompany(reportRequestDto.getCompany());

        processReportRequestService.addCompanyReportRequest(formReportDto);

        log.debug("Finish sending request to form company report, for company: {}", reportRequestDto.getCompany());
    }

    @Override
    public void formCompanyReportWithPriority(GetReportPriorityRequestDto reportRequestDto) {
        log.debug("Start sending request to form company report: {}", reportRequestDto);

        FormReportPriorityDto formReportPriorityDto = new FormReportPriorityDto();
        formReportPriorityDto.setCompany(reportRequestDto.getCompany());
        formReportPriorityDto.setPriority(reportRequestDto.getTariff().getPriority());

        processReportRequestService.addCompanyReportPriorityRequest(formReportPriorityDto);


        log.debug("Finish sending request to form company report: {}", reportRequestDto);

    }

    @Override
    public Set<ReportByCountry> analyzeByCountry(UUID portfolioId) {
        log.debug("Start creating analytics by country for portfolio id: {}", portfolioId);

        PortfolioEntity portfolio = this.portfolioRepository.getById(portfolioId);
        Set<StockEntity> stockEntities = portfolio.getStockEntities();

        Map<String, BigDecimal> countryPercentage = new HashMap<>();

        for (StockEntity stockEntity : stockEntities) {
            String country = stockEntity.getCompany().getCountry().getName();
            BigDecimal fullPrice = stockEntity.getPrice().multiply(BigDecimal.valueOf(stockEntity.getLot()));

            if (countryPercentage.containsKey(country)) {
                BigDecimal price = countryPercentage.get(country).add(fullPrice);
                countryPercentage.put(country, price);
            } else {
                countryPercentage.put(country, fullPrice);
            }
        }

        Set<ReportByCountry> reportByCountrySet = new HashSet<>();

        for (Map.Entry<String, BigDecimal> country : countryPercentage.entrySet()) {
            long percentage = country.getValue().divide(portfolio.getTotalPrice()).longValue() * 100;
            ReportByCountry reportByCountry = new ReportByCountry(country.getKey(), percentage);
            reportByCountrySet.add(reportByCountry);
        }

        log.debug("Finish creating analytics by country for portfolio id: {}, countries number: {}", portfolioId,
                reportByCountrySet.size());

        return reportByCountrySet;
    }
}
