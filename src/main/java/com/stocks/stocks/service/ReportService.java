package com.stocks.stocks.service;

import com.stocks.stocks.dto.ReportByCountry;
import com.stocks.stocks.dto.request.GetReportPriorityRequestDto;
import com.stocks.stocks.dto.request.GetReportRequestDto;

import java.util.Set;
import java.util.UUID;

public interface ReportService {
    /**
     * Form company report.
     *
     * @param reportRequestDto {@link GetReportRequestDto}
     */
    void formCompanyReport(GetReportRequestDto reportRequestDto);

    /**
     * Form company report with priority
     *
     * @param reportRequestDto {@link GetReportPriorityRequestDto}
     */
    void formCompanyReportWithPriority(GetReportPriorityRequestDto reportRequestDto);

    /**
     * Form analytical report by countries.
     *
     * @param portfolioId portfolio Id
     * @return list of {@link ReportByCountry}
     */
    Set<ReportByCountry> analyzeByCountry(UUID portfolioId);
}
