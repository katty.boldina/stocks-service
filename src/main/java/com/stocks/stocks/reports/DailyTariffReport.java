package com.stocks.stocks.reports;

import com.stocks.stocks.dto.Tariff;
import com.stocks.stocks.entities.PortfolioEntity;
import com.stocks.stocks.entities.TariffEntity;
import com.stocks.stocks.repositories.PortfolioRepository;
import com.stocks.stocks.repositories.TariffRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.File;
import java.io.IOException;
import java.math.BigDecimal;
import java.nio.file.Files;
import java.time.LocalDate;
import java.util.List;

@Slf4j
@Service
@RequiredArgsConstructor
public class DailyTariffReport {

    private final PortfolioRepository portfolioRepository;
    private final TariffRepository tariffRepository;
    private final JasperReportService jasperReportService;

    //    @Scheduled(cron = "* * * * * *")
//    @PostConstruct
//    public void formAllTariffsReports() throws IOException {
//        log.info("STARTING CRON");
//
//        long start = System.currentTimeMillis();
//        for (Tariff tariff : Tariff.values()) {
//            TariffReportDto tariffReportDto = createTariffReportDto(tariff);
//            this.formTariffReport(tariffReportDto);
//        }
//        long end = System.currentTimeMillis();
//        log.info("TOTAL TIME CONSUMED: " + (end - start) + " millis");
//
//    }

    public void formTariffReport(TariffReportDto tariffReportDto) throws IOException {
        byte[] report = this.jasperReportService.executeReport(tariffReportDto);
        String fileName = String.format("%s(%s).pdf", tariffReportDto.getTariffName(), LocalDate.now());
        File f = new File("D:/Work/Pet Project/stocks/stocks/src/main/resources/reports/" + fileName);
        Files.write(f.toPath(), report);
    }

//    private TariffReportDto createTariffReportDto(Tariff tariff) {
//        TariffEntity tariffEntity = this.tariffRepository.getByName(tariff).get();
//        List<PortfolioEntity> portfolios = this.portfolioRepository.findAllByTariff(tariffEntity);
//
//        TariffReportDto tariffReportDto = new TariffReportDto();
//        tariffReportDto.setTariffName(tariff.getName());
//        tariffReportDto.setLimit(tariffEntity.getStocksNumber().toString());
//        tariffReportDto.setPriority(tariff.getPriority());
//        tariffReportDto.setPortfoliosNumber(portfolios.size());
//        BigDecimal totalPrice = portfolios.stream().map(PortfolioEntity::getTotalPrice)
//                .reduce(BigDecimal::add).orElse(BigDecimal.valueOf(0));
//        tariffReportDto.setTotalStocksPrice(totalPrice);
//        String template = "tariff-report.jrxml";
//        tariffReportDto.setTemplate(template);
//        tariffReportDto.setCurrency("$");
//        return tariffReportDto;
//    }
}
