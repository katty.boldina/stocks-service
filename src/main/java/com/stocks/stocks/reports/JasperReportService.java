package com.stocks.stocks.reports;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import net.sf.jasperreports.engine.JRAbstractExporter;
import net.sf.jasperreports.engine.JREmptyDataSource;
import net.sf.jasperreports.engine.JasperCompileManager;
import net.sf.jasperreports.engine.JasperFillManager;
import net.sf.jasperreports.engine.JasperPrint;
import net.sf.jasperreports.engine.JasperReport;
import net.sf.jasperreports.engine.export.JRPdfExporter;
import net.sf.jasperreports.export.SimpleExporterInput;
import net.sf.jasperreports.export.SimpleOutputStreamExporterOutput;
import net.sf.jasperreports.export.SimplePdfReportConfiguration;
import org.springframework.core.io.ClassPathResource;
import org.springframework.stereotype.Service;

import javax.persistence.Cacheable;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

@Slf4j
@Service
@RequiredArgsConstructor
public class JasperReportService {
    private static final String REPORT_PATH = "templates/";

    /**
     * Form a report.
     *
     * @param tariffReportDto parameters {@link TariffReportDto}
     * @return byte array
     */
    public byte[] executeReport(TariffReportDto tariffReportDto) {
        Map<String, Object> params = new HashMap<>();
        params.put("priority", tariffReportDto.getPriority());
        params.put("portfoliosNumber", tariffReportDto.getPortfoliosNumber());
        params.put("totalStocksPrice", tariffReportDto.getTotalStocksPrice());
        params.put("limit", tariffReportDto.getLimit());
        params.put("tariffName", tariffReportDto.getTariffName());
        params.put("currency", tariffReportDto.getCurrency());
        return this.createReport(tariffReportDto.getTemplate(), params);
    }

    /**
     * Form and export report.
     *
     * @param template template
     * @param params   params
     * @return byte array
     */
    private byte[] createReport(String template, Map<String, Object> params) {
        ByteArrayOutputStream outputStream = new ByteArrayOutputStream();

        try {
            JasperReport report = getJasperReport(template);
            JasperPrint jasperPrint = JasperFillManager.fillReport(report, params, new JREmptyDataSource());
            if (Objects.isNull(jasperPrint)) {
            }

            JRAbstractExporter exporter = this.getExporter(outputStream, jasperPrint);
            exporter.exportReport();
        } catch (Exception e) {
            log.error("Error occurred: {}", e.getMessage());
        }
        return outputStream.toByteArray();
    }


    /**
     * Get {@link JasperReport}.
     *
     * @param template template name
     * @return {@link JasperReport}
     * @throws Exception
     */
//    @Cacheable("jasperReport")
    public JasperReport getJasperReport(String template) throws Exception {
        InputStream templateInputStream = new ClassPathResource(String.format("%s%s", REPORT_PATH, template)).getInputStream();
        return JasperCompileManager.compileReport(templateInputStream);
    }

    /**
     * Get {@link JRAbstractExporter}.
     *
     * @param outputStream {@link ByteArrayOutputStream}
     * @param jasperPrint  {@link JasperPrint}
     * @return {@link JRAbstractExporter}
     */
    private JRAbstractExporter getExporter(ByteArrayOutputStream outputStream, JasperPrint jasperPrint) {
        JRPdfExporter exporter = this.createPdfExporter();
        exporter.setExporterOutput(new SimpleOutputStreamExporterOutput(outputStream));
        exporter.setExporterInput(new SimpleExporterInput(jasperPrint));
        return exporter;
    }

    /**
     * Create PDF Exporter.
     *
     * @return {@link JRPdfExporter}
     */
    private JRPdfExporter createPdfExporter() {
        JRPdfExporter pdfExporter = new JRPdfExporter();
        SimplePdfReportConfiguration pdfReportConfiguration = new SimplePdfReportConfiguration();
        pdfExporter.setConfiguration(pdfReportConfiguration);
        return pdfExporter;
    }
}
