package com.stocks.stocks.reports;

import com.stocks.stocks.dto.Tariff;
import com.stocks.stocks.entities.PortfolioEntity;
import com.stocks.stocks.entities.TariffEntity;
import com.stocks.stocks.repositories.PortfolioRepository;
import com.stocks.stocks.repositories.TariffRepository;
import lombok.AllArgsConstructor;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.io.IOException;
import java.math.BigDecimal;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

@Service
@AllArgsConstructor
public class GenerateReportsService {

    private final DailyTariffReport dailyTariffReport;
    private final PortfolioRepository portfolioRepository;
    private final TariffRepository tariffRepository;

    @PostConstruct
    public void generateReports() throws IOException {
        ExecutorService executorService = null;
        try {
            executorService = Executors.newFixedThreadPool(3);
            for (Tariff tariff : Tariff.values()) {
                TariffReportDto tariffReportDto = createTariffReportDto(tariff);
                executorService.submit(() -> {
                    try {
                        dailyTariffReport.formTariffReport(tariffReportDto);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                });
            }
        } finally {
            if (executorService != null) executorService.shutdown();
        }
    }

    private TariffReportDto createTariffReportDto(Tariff tariff) {
        TariffEntity tariffEntity = this.tariffRepository.getByName(tariff).get();
        List<PortfolioEntity> portfolios = this.portfolioRepository.findAllByTariff(tariffEntity);

        TariffReportDto tariffReportDto = new TariffReportDto();
        tariffReportDto.setTariffName(tariff.getName());
        tariffReportDto.setLimit(tariffEntity.getStocksNumber().toString());
        tariffReportDto.setPriority(tariff.getPriority());
        tariffReportDto.setPortfoliosNumber(portfolios.size());
        BigDecimal totalPrice = portfolios.stream().map(PortfolioEntity::getTotalPrice)
                .reduce(BigDecimal::add).orElse(BigDecimal.valueOf(0));
        tariffReportDto.setTotalStocksPrice(totalPrice);
        String template = "tariff-report.jrxml";
        tariffReportDto.setTemplate(template);
        tariffReportDto.setCurrency("$");
        return tariffReportDto;
    }

}
