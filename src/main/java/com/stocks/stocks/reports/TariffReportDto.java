package com.stocks.stocks.reports;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

import java.math.BigDecimal;

@Data
@Getter
@Setter
public class TariffReportDto {
    private Short priority;
    private Integer portfoliosNumber;
    private BigDecimal totalStocksPrice;
    private String limit;
    private String tariffName;
    private String template;
    private String currency;
}
