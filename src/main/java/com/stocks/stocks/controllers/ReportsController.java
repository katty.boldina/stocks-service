package com.stocks.stocks.controllers;

import com.stocks.stocks.dto.ReportByCountry;
import com.stocks.stocks.dto.request.GetReportPriorityRequestDto;
import com.stocks.stocks.dto.request.GetReportRequestDto;
import com.stocks.stocks.service.ReportService;
import com.stocks.stocks.service.StockService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Set;
import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/reports")
public class ReportsController {
    private final ReportService reportService;
    private final StockService stockService;

    //ArrayDequeue
    @PostMapping("/company-report")
    @ApiOperation(value = "Запрос на формирование отчёта по компании")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK")
    })
    public ResponseEntity<Void> createCompanyReport(@RequestBody GetReportRequestDto requestDto) {
        reportService.formCompanyReport(requestDto);
        return ResponseEntity.ok().build();
    }

    //priority queue
    @PostMapping("/company-priority-report")
    @ApiOperation(value = "Запрос на формирование отчёта по компании, с учётом приоритета пользователя")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK")
    })
    public ResponseEntity<Void> createCompanyReportWithPriority(@RequestBody GetReportPriorityRequestDto requestDto) {
        reportService.formCompanyReportWithPriority(requestDto);
        return ResponseEntity.ok().build();
    }

    //HashMap анализ портфеля по странам + HashSet
    @GetMapping("/geo-analytics/{portfolioId}")
    @ApiOperation(value = "Запрос на формирование отчёта c процентным соотношением активов по странам")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK")
    })
    public ResponseEntity<Set<ReportByCountry>> analyzeByGeo(@PathVariable UUID portfolioId) {
        return ResponseEntity.ok(reportService.analyzeByCountry(portfolioId));
    }

//    @GetMapping("/test")
//    @ApiResponses(value = {
//            @ApiResponse(code = 200, message = "OK")
//    })
//    public ResponseEntity<byte[]> test() throws IOException {
//        return ResponseEntity.ok(dailyTariffReport.formTariffReport());
//    }

}
