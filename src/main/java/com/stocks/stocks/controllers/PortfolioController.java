package com.stocks.stocks.controllers;

import com.stocks.stocks.dto.AddStocksResponse;
import com.stocks.stocks.dto.StocksToAddDto;
import com.stocks.stocks.service.PortfolioService;
import com.stocks.stocks.service.StockService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.UUID;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/reports")
public class PortfolioController {

    private final PortfolioService portfolioService;
    private final StockService stockService;

    //TreeSet
    //ArrayList
    //Реализовать метод добавления в портфель списка активов. Если у пользователя тариф ESSENTIALS - добавляем
    // 5 активов, если PREMIUM - 10 активов, если ENTERPRISE - добавляем без ограничений.
    // Если пользователь передал больше активов, чем лимит его тарифа, то оставляем только те, которые занимают
    // бОльшую долю порфеля
    @PostMapping("/stocks-to-portfolio/{portfolioId}")
    @ApiOperation(value = "Запрос на добавление активов в портфель")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK")
    })
    public ResponseEntity<AddStocksResponse> addStocksToPortfolio(@PathVariable UUID portfolioId,
                                                                  @RequestBody StocksToAddDto stocksToAddDto) {
        AddStocksResponse addStocksResponse = portfolioService.addStocksToPortfolio(stocksToAddDto, portfolioId);
        return ResponseEntity.ok(addStocksResponse);
    }

    //TreeMap
    //LinkedHashSet
    //LinkedList
}
