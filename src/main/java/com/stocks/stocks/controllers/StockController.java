package com.stocks.stocks.controllers;

import com.stocks.stocks.dto.CreateStockDto;
import com.stocks.stocks.dto.StockInfo;
import com.stocks.stocks.service.ReportService;
import com.stocks.stocks.service.StockService;
import io.swagger.annotations.ApiOperation;
import io.swagger.annotations.ApiResponse;
import io.swagger.annotations.ApiResponses;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/reports")
public class StockController {
    private final ReportService reportService;
    private final StockService stockService;

    //LinkedHashMap
    @GetMapping("/stock/{tickerSymbol}")
    @ApiOperation(value = "Запрос на получение информации по активу")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK")
    })
    public ResponseEntity<StockInfo> getStockInfo(@PathVariable String tickerSymbol) {
        StockInfo stockInfo = this.stockService.getStockInfo(tickerSymbol);
        return ResponseEntity.ok(stockInfo);
    }

    @PostMapping("/stock")
    @ApiOperation(value = "Запрос на создания актива")
    @ApiResponses(value = {
            @ApiResponse(code = 200, message = "OK")
    })
    public ResponseEntity<StockInfo> createStock(@RequestBody CreateStockDto createStockDto) {
        this.stockService.createStock(createStockDto);
        return ResponseEntity.ok().build();
    }
}
