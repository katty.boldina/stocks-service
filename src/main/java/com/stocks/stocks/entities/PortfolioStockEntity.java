package com.stocks.stocks.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "portfolio_stock", schema = "analytics")
public class PortfolioStockEntity implements Serializable {

    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    /**
     * Портфель.
     */
    @ManyToOne
    @JoinColumn(name = "portfolios_id")
    private PortfolioEntity portfolio;

    /**
     * Котируемый инструмент.
     */
    @ManyToOne
    @JoinColumn(name = "stocks_id")
    private StockEntity stock;

    /**
     * Количество лотов
     */
    private int lotAmount;

    /**
     * Итговая стоимость в портфеле: stockAmount * stock.price
     */
    private BigDecimal totalPrice;
}
