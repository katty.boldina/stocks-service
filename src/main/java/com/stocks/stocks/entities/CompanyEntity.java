package com.stocks.stocks.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.UUID;

/**
 * Компания.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "companies", schema = "analytics")
public class CompanyEntity {
    /**
     * Id компании.
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    /**
     * Название компании.
     */
    private String name;

    /**
     * Страна.
     */
    @ManyToOne
    @JoinColumn(name = "country_id")
    private CountryEntity country;
}
