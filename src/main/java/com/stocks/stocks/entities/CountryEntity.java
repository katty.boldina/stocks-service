package com.stocks.stocks.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

/**
 * Страна.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "countries", schema = "analytics")
public class CountryEntity {
    /**
     * Id страны.
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    /**
     * Назвнаие страны.
     */
    private String name;
}
