package com.stocks.stocks.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.util.UUID;

/**
 * Валюта.
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "currencies", schema = "analytics")
public class CurrencyEntity {
    /**
     * Id валюты.
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    /**
     * Название валюты.
     */
    private String name;
}
