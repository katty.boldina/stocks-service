package com.stocks.stocks.entities;


import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToOne;
import javax.persistence.Table;
import javax.persistence.Transient;
import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Set;
import java.util.UUID;

@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "portfolios", schema = "analytics")
public class PortfolioEntity implements Serializable {
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    @ManyToMany
    @JoinTable(
            schema = "analytics",
            name = "portfolio_stock",
            joinColumns = @JoinColumn(name = "portfolios_id"),
            inverseJoinColumns = @JoinColumn(name = "stocks_id")
    )
    private Set<StockEntity> stockEntities;

    private BigDecimal totalPrice;


    @OneToOne
    @JoinColumn(name = "tariff_id")
    private TariffEntity tariff;
}
