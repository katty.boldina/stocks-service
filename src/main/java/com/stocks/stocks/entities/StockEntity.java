package com.stocks.stocks.entities;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.annotations.GenericGenerator;

import javax.persistence.*;
import java.math.BigDecimal;
import java.util.UUID;

/**
 * Котируемый инструмент (акция, облигация, индекс).
 */
@Data
@Entity
@NoArgsConstructor
@AllArgsConstructor
@Table(name = "stocks", schema = "analytics")
public class StockEntity {
    /**
     * Id котируемого инструмента.
     */
    @Id
    @Column(name = "id")
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    private UUID id;

    /**
     * Тикер.
     */
    private String ticker;

    /**
     * Стоимость.
     */
    private BigDecimal price;

    /**
     * Лот.
     */
    private int lot;

    /**
     * Ссылка на компанию эмитент.
     */
    @ManyToOne
    @JoinColumn(name = "company_id")
    private CompanyEntity company;

    /**
     * Ссылка на валюту.
     */
    @ManyToOne
    @JoinColumn(name = "currency_id")
    private CurrencyEntity currency;

    /**
     * Полная стоимость актива в портфеле (стоимость * на к-во лотов)
     */
    @Transient
    private BigDecimal totalPrice;

    /**
     * К-во лотов
     */
    @Transient
    private int lotsNumber;
}
