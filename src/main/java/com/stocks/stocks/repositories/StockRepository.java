package com.stocks.stocks.repositories;


import com.stocks.stocks.entities.StockEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

/**
 * {@link StockRepository} JPA repository.
 */
public interface StockRepository extends JpaRepository<StockEntity, UUID> {

    /**
     * Get stock by ticker.
     *
     * @param tickerSymbol ticker
     * @return {@link StockEntity}
     */
    Optional<StockEntity> findByTicker(String tickerSymbol);

    /**
     * Get all stocks by tickers.
     *
     * @param tickerList tickers
     * @return list of stocks
     */
    List<StockEntity> findAllByTickerIn(List<String> tickerList);

}
