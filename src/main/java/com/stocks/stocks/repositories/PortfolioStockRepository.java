package com.stocks.stocks.repositories;


import com.stocks.stocks.entities.PortfolioEntity;
import com.stocks.stocks.entities.PortfolioStockEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

/**
 * {@link PortfolioStockRepository} JPA repository.
 */
public interface PortfolioStockRepository extends JpaRepository<PortfolioStockEntity, UUID> {

    /**
     * Get all {@link PortfolioStockEntity} by portfolio.
     *
     * @param portfolioEntity {@link PortfolioEntity}
     * @return list of {@link {@link PortfolioStockEntity}}
     */
    List<PortfolioStockEntity> getAllByPortfolio(PortfolioEntity portfolioEntity);

}
