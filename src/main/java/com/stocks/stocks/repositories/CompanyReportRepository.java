package com.stocks.stocks.repositories;

import com.stocks.stocks.entities.CompanyReportEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.UUID;

/**
 * {@link CompanyReportEntity} JPA repository.
 */
public interface CompanyReportRepository extends JpaRepository<CompanyReportEntity, UUID> {

}
