package com.stocks.stocks.repositories;


import com.stocks.stocks.entities.PortfolioEntity;
import com.stocks.stocks.entities.TariffEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;
import java.util.UUID;

/**
 * {@link PortfolioEntity} JPA repository.
 */
public interface PortfolioRepository extends JpaRepository<PortfolioEntity, UUID> {

    List<PortfolioEntity> findAllByTariff(TariffEntity tariffEntity);

}
