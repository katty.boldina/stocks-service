package com.stocks.stocks.repositories;

import com.stocks.stocks.dto.Tariff;
import com.stocks.stocks.entities.TariffEntity;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.Optional;
import java.util.UUID;

/**
 * {@link TariffRepository} JPA repository.
 */
public interface TariffRepository extends JpaRepository<TariffEntity, UUID> {
    /**
     * Get tariff by name.
     *
     * @param name tariff name
     * @return {@link TariffEntity}
     */
    Optional<TariffEntity> getByName(Tariff name);

}
