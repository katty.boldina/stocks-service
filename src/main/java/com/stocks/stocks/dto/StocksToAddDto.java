package com.stocks.stocks.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class StocksToAddDto {
    List<StockToAdd> stocksToAdds;
}
