package com.stocks.stocks.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.util.List;

@Data
@AllArgsConstructor
public class AddStocksResponse {
    @ApiModelProperty("Данные портфеля")
    private Portfolio portfolio;
    @ApiModelProperty("Добавленные активы")
    private List<AddedStocks> addedStocks;
    @ApiModelProperty("Недобавленные активы")
    private List<FailedToAddStock> failedToAddStocks;
}
