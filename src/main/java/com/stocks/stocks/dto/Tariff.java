package com.stocks.stocks.dto;

import lombok.Getter;

public enum Tariff {
    ESSENTIALS((short) 3, "Essentials"),
    PREMIUM((short) 2, "Premium"),
    ENTERPRISE((short) 1, "Enterprise");

    @Getter
    private short priority;
    @Getter
    private String name;

    Tariff(short priority, String name) {
        this.name = name;
        this.priority = priority;
    }
}
