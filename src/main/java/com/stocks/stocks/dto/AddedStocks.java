package com.stocks.stocks.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class AddedStocks {
    @ApiModelProperty("Тикер")
    private String ticker;
    @ApiModelProperty("Количество лотов")
    private int lotsNumber;
}
