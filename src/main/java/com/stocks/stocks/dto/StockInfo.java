package com.stocks.stocks.dto;

import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.math.BigDecimal;

@Data
@Slf4j
public class StockInfo {

    private String tickerSymbol;
    private BigDecimal price;
    private int lot;
    private String country;

    public StockInfo(String tickerSymbol, BigDecimal price, int lot, String country) {
        log.info("Creating StockInfo via constructor");
        this.tickerSymbol = tickerSymbol;
        this.price = price;
        this.lot = lot;
        this.country = country;
    }
}
