package com.stocks.stocks.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class StockToAdd {
    private String ticker;
    private int lotsNumber;
}
