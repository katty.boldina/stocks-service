package com.stocks.stocks.dto.request;

import com.stocks.stocks.dto.Company;
import lombok.Data;

@Data
public class GetReportRequestDto {
    private Company company;
}
