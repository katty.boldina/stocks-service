package com.stocks.stocks.dto.request;

import com.stocks.stocks.dto.Company;
import com.stocks.stocks.dto.Tariff;
import lombok.Data;

@Data
public class GetReportPriorityRequestDto {
    private Company company;
    private Tariff tariff;
}
