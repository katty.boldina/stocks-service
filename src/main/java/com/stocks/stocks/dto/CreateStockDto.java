package com.stocks.stocks.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;

@Data
@AllArgsConstructor
public class CreateStockDto {

    private String tickerSymbol;
    private BigDecimal price;
    private int lot;
    private String country;
}
