package com.stocks.stocks.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

import java.math.BigDecimal;
import java.util.UUID;

@Data
@AllArgsConstructor
public class Portfolio {
    @ApiModelProperty("Id портфеля")
    private UUID id;
    @ApiModelProperty("Общая стоимость порфеля")
    private BigDecimal totalPrice;
    @ApiModelProperty("Наименование тарифа")
    private Tariff tariff;
    @ApiModelProperty("Максимально возможное количество активов по тарифу")
    private int tariffStockNumber;
    @ApiModelProperty("Текущее количество активов по тарифу")
    private int currentStockNumber;
}
