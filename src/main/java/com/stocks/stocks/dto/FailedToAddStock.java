package com.stocks.stocks.dto;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class FailedToAddStock {
    @ApiModelProperty("Тикер")
    private String ticker;
    @ApiModelProperty("Количество лотов")
    private int lotsNumber;
    @ApiModelProperty("Причина недобавления актива в портфель")
    private String reason;
}
