package com.stocks.stocks.dto;

import lombok.AllArgsConstructor;
import lombok.Data;

@Data
@AllArgsConstructor
public class ReportByCountry {
    private String country;
    private long percentage;
}
