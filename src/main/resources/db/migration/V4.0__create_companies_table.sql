create table if not exists analytics.companies
(
    id         uuid primary key,
    name       varchar not null,
    country_id uuid references analytics.countries (id)
);

comment on table analytics.companies is '���������� ��������';
comment on column analytics.companies.id is 'Id ��������-��������';
comment on column analytics.companies.name is '�������� ��������';
comment on column analytics.companies.country_id is '������ �� ������ (����������)';
