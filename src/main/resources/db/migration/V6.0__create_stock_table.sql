create table if not exists analytics.stocks
(
    id          uuid primary key,
    ticker      varchar unique not null,
    price       numeric        not null,
    lot         int            not null,
    company_id  uuid references analytics.companies (id),
    currency_id uuid references analytics.currencies (id)
);

comment on table analytics.stocks is '���������� ���������� (�����, ���������, ������)';
comment on column analytics.stocks.id is 'Id ����������� �����������';
comment on column analytics.stocks.ticker is '�����';
comment on column analytics.stocks.price is '���������';
comment on column analytics.stocks.lot is '���';
comment on column analytics.stocks.company_id is '�������� �������';
comment on column analytics.stocks.currency_id is '������';