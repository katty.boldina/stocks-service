create table if not exists analytics.tariffs
(
    id            uuid primary key,
    name          varchar unique not null,
    stocks_number integer        not null
);

comment on table analytics.tariffs is '�����';
comment on column analytics.tariffs.id is 'Id ������';
comment on column analytics.tariffs.name is '������������ ������';
comment on column analytics.tariffs.stocks_number is '���������� �������';
