create table if not exists analytics.analytics.portfolios
(
    id          uuid primary key,
    total_price numeric not null,
    tariff_id   uuid references analytics.analytics.tariffs (id)
);

INSERT INTO analytics.analytics.portfolios (id, total_price, tariff_id)
VALUES ('660caf3c-e023-44c9-8ca7-88541e519fc3','76345','8dcbdd4a-2e98-4343-affa-4c000cdb5077'),
--        ('0c73012d-4481-4521-8d37-757aae917e87','2345','da7d3a7c-5c74-11ec-bf63-0242ac130002'),
--        ('bd6a2a5a-b6cd-4826-b5bc-dc1969fc243c','54','da7d381a-5c74-11ec-bf63-0242ac130002'),
--        ('9bffb3f0-cca9-42f0-9ce5-87475a23bee3','3452','da7d3a7c-5c74-11ec-bf63-0242ac130002'),
--        ('135d7359-87f6-4d95-9301-06e8e267e655','352','da7d381a-5c74-11ec-bf63-0242ac130002'),
--        ('cf8b1469-19e0-4e66-8961-2db9f9bde8c1','629','da7d381a-5c74-11ec-bf63-0242ac130002'),
--        ('6b75f8fa-91c7-4ee2-9bce-c185fb5a7696','9246','da7d3a7c-5c74-11ec-bf63-0242ac130002'),
--        ('d6f0f81c-a820-44f8-9c38-41a59727e71e','7823','da7d3a7c-5c74-11ec-bf63-0242ac130002'),
--        ('12242484-26a5-4511-8a36-ef1535b63944','13905','da7d3b94-5c74-11ec-bf63-0242ac130002'),
--        ('33497800-0e38-4ce3-9f3b-1ce525ea6c67','8364','da7d3a7c-5c74-11ec-bf63-0242ac130002'),
--        ('eec7652e-f717-47f1-a5af-e09e7da62260','9024','da7d3a7c-5c74-11ec-bf63-0242ac130002'),
--        ('30737279-2916-47d7-a597-c508de331d25','64372','da7d3b94-5c74-11ec-bf63-0242ac130002'),
--        ('3979234d-40c8-443e-9c67-6b4fc0c55f42','12471','da7d3b94-5c74-11ec-bf63-0242ac130002'),
--        ('ae3ba096-c973-40bf-bf50-f5f2e6d5e3c7','73276','da7d3b94-5c74-11ec-bf63-0242ac130002'),
--        ('5caeec52-1a79-4f39-97a3-8c405d0ff3a0','762','da7d381a-5c74-11ec-bf63-0242ac130002'),
--        ('988a032d-bdef-4742-a9f2-dd7ea628ce4a','26251','da7d3b94-5c74-11ec-bf63-0242ac130002'),
--        ('b41c678d-433b-4b7e-b034-612e9b534591','1762456','da7d3b94-5c74-11ec-bf63-0242ac130002'),
--        ('e49f1201-6daf-4cb7-a168-1d8897f89539','23652','da7d3b94-5c74-11ec-bf63-0242ac130002'),
       ('5924d593-2a40-4a08-838d-4f5560bdc955','3846','8dcbdd4a-2e98-4343-affa-4c000cdb5077'),
       ('8562f24b-7b06-491f-b716-b97c94025616','9568','5a1fda7a-48e5-4e11-9f51-48d4cecd997a');
