create table analytics.portfolio_stock
(
    id           uuid primary key,
    portfolios_id uuid    not null
        constraint portfolio_fk
            references analytics.portfolios,
    stocks_id     uuid    not null
        constraint stock_fk
            references analytics.stocks,
    lot_amount   integer not null,
    total_price  numeric not null
);