create table if not exists analytics.portfolios
(
    id          uuid primary key,
    total_price numeric not null,
    tariff_id   uuid references analytics.tariffs (id)
);
